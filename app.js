var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var bodyParser = require('body-parser');
var app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);
var session = require('express-session');
const port = 3000;


// view engine setup
app.set('views','./views');
app.use(express.static(__dirname + '/public'));
app.use('/liveblog/:id',express.static(__dirname + '/public'));
app.set('view engine','ejs');



app.use(bodyParser.urlencoded({
  extended:true
}));

app.use(session({ secret: 'promo-code',resave: true, saveUninitialized:true})); // session secret


app.use((req,res,next)=> {
  app.locals.user = req.session.user;
  next();
});

require('./routes/index')(io,app);
require('./routes/users')(io,app);

app.use(function(req, res, next) {
  next(createError(404));
});



http.listen(3000,()=>{
  console.log("Started LiveBlogging App on port: "+3000);
});



module.exports = app;
