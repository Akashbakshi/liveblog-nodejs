
module.exports =  (io,app)=> {
  const bcrypt = require('bcrypt');
  var passport   = require('passport');

  app.use(passport.initialize());

  app.use(passport.session()); // persistent login sessions

  var pool = require('../config/db.js');




  app.get('/logout',(req,res)=>{
      req.session.destroy();
      res.redirect('/');
  });
  app.get('/Broadcast',async (req,res)=>{
    var authors = [];

    let authorRaw = await pool.query('select * from tb_author');
    let authorArr = await Object.values(JSON.parse(JSON.stringify(authorRaw)));

    authorArr.forEach((author)=>{
        authors.push(author);
    });

    res.render('live_broadcast',{authors:authors});
  });

  app.post('/Broadcast',  async(req, res) =>{
    var pubAuth="";
    var time;

    var content = req.body.content;
    var authSel = req.body.author;


    let current_datetime = new Date();
    let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
    dbTime = formatted_date;
    //dbTime = dbTime.replace('Z',' ');
    time = current_datetime.toLocaleString('en-US', { hour: 'numeric',minute:'numeric',second:'numeric', hour12: true });
    let insertQuery = "INSERT INTO `liveblogDB`.`tb__feed`\n" +
        "(`content`,\n" +
        "`fk__author`,\n" +
        "`created_at`,\n" +
        "`fk__event`)\n" +
        "VALUES\n" +
        "(\""+content+"\",\n" +
        ""+authSel+",\n" +
        "'"+dbTime+"',\n" +
        "1);\n";


    pool.query(insertQuery);
    let authors = await pool.query("SELECT * from tb_author");

    let authorArr = await Object.values(JSON.parse(JSON.stringify(authors)));

    authorArr.forEach((author)=>{
      if(author.pk__id == authSel){
        pubAuth = author.name;
      }
    });

    io.emit('update',content,pubAuth,time);

    res.redirect('/broadcast');
  });


  app.get('/LivePost',(req,res)=>{
      res.render('create_liveblog');
  });

  app.post('/CreateLiveBlog',async (req,res)=>{
      let title = req.body.title;
      let startDate = req.body.date;

      let insertQuery = "INSERT INTO tb_liveblog(name,event_date) VALUES("+"'"+title+"','"+startDate+"')";

      pool.query(insertQuery);

      let posts = await pool.query("select * from tb_liveblog");
      let postsArr = await Object.values(JSON.parse(JSON.stringify(posts)));

      req.session.posts = postsArr;

      res.redirect('/');
      console.log(insertQuery);
  });

  app.get('/registration',(req,res)=>{
      res.render('register');
  });

  app.post('/registerUser',async (req,res)=>{
      let username = req.body.username;
      let rawPassword=  req.body.password;
      let email = req.body.email;
      let password = await bcrypt.hashSync(rawPassword,14);

      let insertQuery = "INSERT INTO tb_users(username,password,email) VALUES ("+"'"+username+"','"+password+"','"+email+"')";


      pool.query(insertQuery);
      res.redirect('/');
  });

  app.get('/login',  (req,res)=>{
    res.render('login',{error:req.session.errMsg});

    req.session.errMsg = null;
  });

  app.post('/login',async (req,res)=>{
    let username = req.body.username;
    let password = req.body.password;

    let getPassQuery="select password from tb_users where username ="+"'"+username+"'";

    let hashPassword = await pool.query(getPassQuery);


    if(hashPassword.length>0){
      if(bcrypt.compareSync(password,hashPassword[0].password)){

          req.session.user = username;

        req.session.errMsg = null;
        res.redirect('/');

      }else{
        req.session.errMsg = "Username/Password Combination Is Wrong";
        res.redirect('/login');
      }
    }else{

      req.session.errMsg = "Username/Password Combination Is Wrong";
      res.redirect('/login');
    }

  });

}