
module.exports = (io,app)=>{


    var pool = require('../config/db.js');
    var query = require('../config/queries.json');


    /* GET home page. */
    app.get('/', (req, res, next)=> {

        res.render('index',{title:"Live Blogger - Homepage"});
    });

    app.get('/allliveblogs',async (req,res)=>{

        var allPosts = [];
        try{
            let allPostsRaw = await pool.query(query.GetAllLiveblogs);
            let allPostsArr = await Object.values(JSON.parse(JSON.stringify(allPostsRaw)));
            allPostsArr.forEach((post)=>{
               allPosts.push(post);
            });


        }catch(err){
            console.log(err);
        }


        res.render('list_liveblogs',{title:"Live Blogger - All Live Blogs",posts:allPosts});
    });

    app.get('/liveblog/:id',async (req,res)=>{
        var postID = req.params.id;
        var contentArr = [];
        var postInfo;

        try{
            let finalQuery = query.feedQuery1+postID+query.feedQuery2;
            let feedRaw = await pool.query(finalQuery);
            let feedArr = await Object.values(JSON.parse(JSON.stringify(feedRaw)));

            feedArr.forEach((feedItem)=>{
                contentArr.push(feedItem);
            });

        }catch(err){
            console.log(err);
        }


        try{
            let blogId = 1;
            let liveblogRaw = await pool.query(query.liveBlogQuery+blogId);
            postInfo = await Object.values(JSON.parse(JSON.stringify(liveblogRaw)));
            console.log(postInfo[0].name);
        }catch(err){
            console.log(err);
        }
        res.render('liveblog', {title: postInfo[0].name,content:contentArr,info: postInfo});
    });



    io.on('connection',(socket)=>{
        console.log("connected");
    });

};
