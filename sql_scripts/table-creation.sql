create schema if not exists liveblogDB collate utf8mb4_0900_ai_ci;

create table if not exists tb__feed
(
	pk_id int auto_increment,
	content varchar(1024) not null,
	fk__author int not null,
	created_at datetime not null,
	fk__event int not null,
	constraint pk_id_UNIQUE
		unique (pk_id)
);

alter table tb__feed
	add primary key (pk_id);

create table if not exists tb_author
(
	pk__id int auto_increment
		primary key,
	name varchar(1024) not null,
	fk_users int not null
);

create table if not exists tb_liveblog
(
	pk_id int auto_increment,
	name varchar(256) not null,
	event_date date not null,
	constraint pk_id_UNIQUE
		unique (pk_id)
);

alter table tb_liveblog
	add primary key (pk_id);


create table if not exists tb_users
(
	pk_id int auto_increment
		primary key,
	username varchar(256) not null,
	password varchar(512) not null,
	email varchar(512) not null,
	constraint tb_users_username_uindex
		unique (username)
);

